<div align="center" >
  <a href="#">
    <img src="/public/e2pay2x.png" alt="e2pay" width="480px">
  </a>
</div>

## Noted

- responsive application for mobile and website
- This application has been cleaned of unused source code
- Color Mode
- and many other advantages

## Getting Started Frontend - React

```bash
cd e2pay
```

First, install all needed dependencies:

```bash
npm install
```

Then, run the development server:

```bash
npm start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## More

take a look at the following resources:

- [React](https://reactjs.org/)
- [Chakra UI](https://chakra-ui.com/)
- [React-router-dom](https://www.npmjs.com/package/react-router-dom)
- [Typescript](#)
- [etc](#)
