import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./HomePage";
import NotFound from "./404";
import Payment from "./Payment";

export function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/Payment" element={<Payment />} />

        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}
