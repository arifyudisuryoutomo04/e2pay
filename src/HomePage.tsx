import { Button, Center, Container, Link } from "@chakra-ui/react";
import Hero from "./components/hero";
import Profil from "./components/profil";

const HomePage = () => {
  return (
    <>
      <Hero />
      <Profil />
      <Container maxW={"xxl"} p="12">
        <Center>
          <Link
            href="/Payment"
            _hover={{
              textDecoration: "none"
            }}
          >
            <Button
              size="md"
              height="48px"
              width="200px"
              border="2px"
              borderColor="green.500"
            >
              Payment
            </Button>
          </Link>
        </Center>
      </Container>
    </>
  );
};

export default HomePage;
