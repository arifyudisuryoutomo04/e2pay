import {
  ArrowForwardIcon,
  ChevronDownIcon,
  ChevronLeftIcon,
  LockIcon,
  ViewIcon,
  ViewOffIcon
} from "@chakra-ui/icons";
import {
  Container,
  Stack,
  useColorModeValue,
  Text,
  Image,
  Box,
  Center,
  Heading,
  Link,
  Popover,
  Flex,
  PopoverTrigger,
  PopoverContent,
  Button,
  PopoverCloseButton,
  TabList,
  PopoverBody,
  PopoverHeader,
  PopoverArrow,
  Tabs,
  TabPanels,
  TabPanel,
  Grid,
  GridItem,
  Divider,
  Tab,
  FormLabel,
  FormControl,
  Input,
  HStack,
  InputGroup,
  InputRightElement,
  Checkbox
} from "@chakra-ui/react";
import React, { useState } from "react";
//? Import data json {fetching data}
import Data from "./data/data.json";

function Payment() {
  const [showPassword, setShowPassword] = useState(false);

  return (
    <Stack color={"black"}>
      <Container maxW={"7xl"} my={"40px"}>
        <Box
          marginTop={{ base: "1", sm: "5" }}
          display="flex"
          flexDirection={{ base: "column", sm: "row" }}
          justifyContent="space-between"
        >
          <Box
            display="flex"
            flex="1"
            rounded="xl"
            position="relative"
            alignItems="center"
          >
            <Box
              width={{ base: "100%", sm: "85%" }}
              zIndex="2"
              marginLeft={{ base: "0", sm: "5%" }}
            >
              <Stack p={3} textAlign="center">
                <Stack>
                  <Center>
                    <Image
                      borderRadius="full"
                      src="/Logo-tiket-com.png"
                      alt="logo"
                    />
                  </Center>
                  <Text>Tiket.com / PT Global Tiket Network</Text>
                </Stack>
                <Stack pt={5}>
                  <Text color={"blue"}>Total Payment</Text>
                  <Heading color={"blue"}>2,000,000.00</Heading>
                  <Text color={"gray.400"}>Invoice code: 0123456789AM</Text>
                </Stack>
                <Box pt={3}>
                  <Flex justifyContent="center" my={4}>
                    <Popover placement="bottom" isLazy>
                      <PopoverTrigger>
                        <Button
                          borderRadius="full"
                          rightIcon={<ChevronDownIcon />}
                          bg="white"
                          w="fit-content"
                        >
                          <Box w="30rem">
                            <Text float={"left"}>Detail</Text>
                            <Text float={"right"}>4 Items</Text>
                          </Box>
                        </Button>
                      </PopoverTrigger>
                      <PopoverContent
                        _focus={{ boxShadown: "none" }}
                        w="23rem"
                        h="full"
                      >
                        <PopoverArrow />
                        <PopoverCloseButton
                          color={useColorModeValue("black", "white")}
                        />
                        <PopoverHeader
                          fontWeight="bold"
                          color={useColorModeValue("black", "white")}
                        >
                          Detail
                        </PopoverHeader>
                        <PopoverBody
                          w="full"
                          h="full"
                          color={useColorModeValue("black", "white")}
                        >
                          {Data &&
                            Data.map((e, index) => {
                              return (
                                <Link
                                  key={index + 1}
                                  href="#"
                                  _hover={{
                                    textDecoration: "none"
                                  }}
                                >
                                  <Box mb={4}>
                                    <Grid
                                      templateColumns="repeat(4, 1fr)"
                                      gap={2}
                                    >
                                      <GridItem colSpan={3} textAlign="left">
                                        <Text as={"b"}>{e.name_tikect}</Text>
                                        <br />
                                        <Text
                                          fontSize={"xl"}
                                          color="blue"
                                          as={"b"}
                                        >
                                          Rp. {e.price}
                                        </Text>
                                      </GridItem>
                                      <GridItem>
                                        <Box width={"100%"}>
                                          <Text float={"left"} color="gray.300">
                                            Qty
                                          </Text>
                                          <Text float={"right"}>
                                            {e.amount_ticket}
                                          </Text>
                                        </Box>
                                      </GridItem>
                                    </Grid>
                                  </Box>
                                </Link>
                              );
                            })}
                        </PopoverBody>
                      </PopoverContent>
                    </Popover>
                  </Flex>
                </Box>
              </Stack>
            </Box>
            <Box
              zIndex="1"
              width="100%"
              position="absolute"
              height="100%"
              bg={"gray.200"}
              roundedLeft="2rem"
            ></Box>
          </Box>
          <Box
            bg={"white"}
            flex="1"
            flexDirection="column"
            justifyContent="center"
            p={3}
            width={{ base: "100%", sm: "50%" }}
            roundedRight="2rem"
          >
            <Stack>
              <Box mx={4}>
                <Stack float={"left"} pt={"18px"}>
                  <Link
                    href="/"
                    style={{
                      textDecoration: "none"
                    }}
                  >
                    <Button
                      leftIcon={<ChevronLeftIcon />}
                      bg="transaprent"
                      variant="solid"
                      color={"blue"}
                    >
                      Back
                    </Button>
                  </Link>
                </Stack>
                <Stack float={"right"} my={2}>
                  <Heading>
                    <Link
                      textDecoration="none"
                      _hover={{ textDecoration: "none" }}
                    >
                      Credit Card
                    </Link>
                  </Heading>
                </Stack>
              </Box>
            </Stack>
            <Stack borderColor={"gray.500"} my={5}>
              <Divider orientation="horizontal" />
            </Stack>
            <Stack mt={3}>
              <Tabs variant="soft-rounded" colorScheme="green" isFitted>
                <Center>
                  <TabList w="90%" mb={6}>
                    <Tab>SAVED CARD</Tab>
                    <Tab>NEW CARD</Tab>
                  </TabList>
                </Center>
                <TabPanels>
                  <TabPanel height={"28rem"}>
                    <FormControl id="email" isRequired>
                      <FormLabel>Select Card</FormLabel>
                      <Box>
                        <Flex justifyContent="center" mt={4}>
                          <Popover placement="bottom" isLazy>
                            <PopoverTrigger>
                              <Button
                                borderRadius="full"
                                rightIcon={<ChevronDownIcon />}
                                border="1px"
                                borderColor="gray.200"
                                bg="gray.100"
                                w="fit-content"
                              >
                                <Box w="40rem">
                                  <Image
                                    float={"left"}
                                    mr={1}
                                    objectFit="cover"
                                    src="/58482363cef1014c0b5e49c1.png"
                                  />
                                  <Text float={"left"} marginTop={"-2px"}>
                                    * 6677
                                  </Text>
                                  <Text float={"right"}>12/24</Text>
                                </Box>
                              </Button>
                            </PopoverTrigger>
                            <PopoverContent
                              _focus={{ boxShadown: "none" }}
                              h="full"
                            >
                              <PopoverArrow />
                              <PopoverCloseButton
                                color={useColorModeValue("black", "white")}
                              />
                              <PopoverHeader
                                fontWeight="bold"
                                color={useColorModeValue("black", "white")}
                              >
                                Blank Data
                              </PopoverHeader>
                            </PopoverContent>
                          </Popover>
                        </Flex>
                        <Stack my={3}>
                          <HStack mb={"2rem"}>
                            <Grid templateColumns="repeat(3, 1fr)" gap={6}>
                              <GridItem w="100%">
                                <Box>
                                  <FormControl id="CVV" isRequired>
                                    <FormLabel>CVV</FormLabel>
                                    <Input
                                      type="text"
                                      borderRadius="full"
                                      border="1px"
                                      placeholder=" . . ."
                                      focusBorderColor="gray.200"
                                      borderColor="gray.200"
                                      bg="gray.100"
                                    />
                                  </FormControl>
                                </Box>
                              </GridItem>
                              <GridItem w="100%" colSpan={2}>
                                <Box>
                                  <FormControl id="password" isRequired>
                                    <FormLabel>Password</FormLabel>
                                    <InputGroup>
                                      <Input
                                        focusBorderColor="gray.200"
                                        borderRadius="full"
                                        border="1px"
                                        borderColor="gray.200"
                                        bg="gray.100"
                                        w={"100% "}
                                        type={
                                          showPassword ? "text" : "password"
                                        }
                                      />
                                      <InputRightElement h={"full"}>
                                        <Button
                                          variant={"ghost"}
                                          onClick={() =>
                                            setShowPassword(
                                              (showPassword) => !showPassword
                                            )
                                          }
                                        >
                                          {showPassword ? (
                                            <ViewIcon color={"black"} />
                                          ) : (
                                            <ViewOffIcon color={"black"} />
                                          )}
                                        </Button>
                                      </InputRightElement>
                                    </InputGroup>
                                  </FormControl>
                                  <Stack
                                    direction={{ base: "column", sm: "row" }}
                                    align={"start"}
                                    justify={"space-between"}
                                  >
                                    <Link color={"blue.400"}>
                                      Forgot password?
                                    </Link>
                                  </Stack>
                                </Box>
                              </GridItem>
                            </Grid>
                          </HStack>
                        </Stack>
                        <Stack mt={"40px"}>
                          <Checkbox colorScheme="green">
                            I agree E2Pay will charge the stated amount on my
                            credit card
                          </Checkbox>
                        </Stack>
                      </Box>
                    </FormControl>
                    <Stack my={5}>
                      <Button
                        _hover={{}}
                        rounded={"full"}
                        rightIcon={<ArrowForwardIcon color={"green.500"} />}
                        bg={"blue"}
                        color="white"
                      >
                        PROCEED TO PAYMENT
                      </Button>
                    </Stack>
                    <Stack textAlign={"center"} color="red">
                      <Link href="#">
                        <LockIcon mx="2px" /> Your payment is protected &
                        processed by E2Pay
                      </Link>
                    </Stack>
                  </TabPanel>
                  <TabPanel height={"28rem"}>
                    <p>New Card!</p>
                  </TabPanel>
                </TabPanels>
              </Tabs>
            </Stack>
          </Box>
        </Box>
      </Container>
    </Stack>
  );
}

export default Payment;
