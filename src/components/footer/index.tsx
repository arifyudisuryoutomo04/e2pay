import {
  Box,
  Container,
  SimpleGrid,
  Stack,
  Text,
  useColorModeValue,
  Image,
  HStack
} from "@chakra-ui/react";

export default function LargeWithNewsletter() {
  return (
    <Box bg={useColorModeValue("blue", "gray.900")} color="gray.200">
      <Container as={Stack} maxW={"7xl"} py={10}>
        <SimpleGrid columns={{ base: 1, sm: 2, md: 2 }} spacing={8}>
          <Stack>
            <Image src="/e2pay2x.png" alt="logo" width={"20rem"} />
          </Stack>
          <Stack>
            <HStack justifyContent={"right"}>
              <Stack>
                <Text
                  style={{
                    float: "left"
                  }}
                >
                  <Image
                    paddingRight={2}
                    style={{
                      float: "left"
                    }}
                    borderRadius="full"
                    src="/telp.png"
                  />
                  021-5292-0138 (Mon - Fri, 08:30 - 18:00)
                </Text>
              </Stack>
              <Stack>
                <Text
                  style={{
                    float: "left"
                  }}
                >
                  <Image
                    paddingRight={2}
                    style={{
                      float: "left"
                    }}
                    borderRadius="full"
                    src="/email.png"
                  />
                  Suport@e2pay.co.id
                </Text>
              </Stack>
            </HStack>

            <Stack textAlign={"right"}>
              <Text fontSize={"sm"}>
                Copyright © 2022, PT E2Pay Global Utama. All rights reserved
              </Text>
            </Stack>
          </Stack>
        </SimpleGrid>
      </Container>
    </Box>
  );
}
