import { ChakraProvider } from "@chakra-ui/react";
import { ColorModeScript } from "@chakra-ui/color-mode";
import * as ReactDOM from "react-dom/client";
import { App } from "./App";
import Footer from "./components/footer";
import Navbar from "./components/navbar";
import * as React from "react";
import theme from "./components/theme";

const container = document.getElementById("root");
if (!container) throw new Error("Failed to find the root element");
const root = ReactDOM.createRoot(container);

root.render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <ColorModeScript initialColorMode={theme.config.initialColorMode} />
      <Navbar />
      <App />
      <Footer />
    </ChakraProvider>
  </React.StrictMode>
);
